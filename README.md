# Across The Atlantic
## Game Rules
1.Use 'W' 'A' 'S' 'D' for navigation.  
2.'R' for checking game result at any time.  
3.A Player wins if the other player dies or his finalscore(obstacles and time)  is greater than the other opponent.  
4.Passing fixed obstacles means 5 points,moving obstacles mean 10.  
5.The greater the time taken, greater is the reduction in finalscore.   
6.At any point in the game the one with the highest level is the current.  
7.Run game.py to run the game

# Press Any key To Enter The Game
