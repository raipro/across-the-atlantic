
#messages
txt1 = "PLAYER 1 WINS THE ROUND"
txt2 = "PLAYER 2 WINS THE ROUND"
txt3 = "PLAYER1 IS WINNING"
txt4 = "PLAYER2 IS WINNING"
txt5 = "DRAW"
txt6 = "PLAYER 2"
txt7 = "BOTH WIN THE ROUND"
txt8 = "YOU BOTH LOSE THE ROUND"
txt9 = "PLAYER 1"

#fonts
fonttype =  'freesansbold.ttf'
fontsize1 = 32
fontsize2 = 40
fontsize3 = 24

#globalvariables
playerX_change = 0
playerY_change = 0
score = 0
count10 = 0
textX = 10
textY = 10
level1 = 1
level2 = 1
run = True
start_time = 0
x1 = 1
a = 1
flag = 0
flag1 = 0
homepage = 1

#colours
black = (0,0,0)
yellow = (255,255,0)
blue = (0,0,255)
