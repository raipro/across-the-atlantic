import pygame
import math
from config import *

pygame.init()

screen = pygame.display.set_mode((800, 800))

# icon
icon = pygame.image.load('ocean.png')
pygame.display.set_icon(icon)
# background
background = pygame.image.load('background.png')
background = pygame.transform.scale(background, (800, 750))
# player
playerimg = pygame.image.load('boat.png')
playerimg = pygame.transform.scale(playerimg, (32, 32))
playerX = 370
playerY = 720

# images
obstacle1img = pygame.image.load('ocean.png')
obstacle1img = pygame.transform.scale(obstacle1img, (32, 32))
obstacle1X = 0
obstacle1Y = 640

img11 = obstacle1img
img12 = obstacle1img
img13 = obstacle1img
img14 = obstacle1img

obstacle2img = pygame.image.load('ice.png')
obstacle2img = pygame.transform.scale(obstacle2img, (32, 32))
obstacle2X = 200
obstacle2Y = 640

img21 = obstacle2img
img22 = obstacle2img
img23 = obstacle2img
img24 = obstacle2img

obstacle3img = pygame.image.load('plank.png')
obstacle3img = pygame.transform.scale(obstacle3img, (32, 32))
obstacle3X = 400
obstacle3Y = 640

img31 = obstacle3img
img32 = obstacle3img
img33 = obstacle3img
img34 = obstacle3img

obstacle4img = pygame.image.load('trash.png')
obstacle4img = pygame.transform.scale(obstacle4img, (32, 32))
obstacle4X = 600
obstacle4Y = 640

img41 = obstacle4img
img42 = obstacle4img
img43 = obstacle4img
img44 = obstacle4img

obstacle5img = pygame.image.load('mountain.png')
obstacle5img = pygame.transform.scale(obstacle5img, (32, 32))
obstacle5X = 200
obstacle5Y = 580

img51 = obstacle5img
img52 = obstacle5img
img53 = obstacle5img

obstacle6img = pygame.image.load('stone.png')
obstacle6img = pygame.transform.scale(obstacle6img, (32, 32))
obstacle6X = 450
obstacle6Y = 580

img61 = obstacle6img
img62 = obstacle6img
img63 = obstacle6img

# fonts
font = pygame.font.Font(fonttype, fontsize1)
font1 = pygame.font.Font(fonttype, fontsize2)
font2 = pygame.font.Font(fonttype, fontsize3)

# functions


def show_result(lvl1, lvl2):
    screen.blit(background, (0, 0))
    player1lvl = font.render("PLayer1: level " + str(lvl1), True, (0, 0, 0))
    player2lvl = font.render("PLayer2: level " + str(lvl2), True, (0, 0, 0))
    screen.blit(player1lvl, (10, 200))
    screen.blit(player2lvl, (10, 400))
    if lvl1 > lvl2:
        show_text(txt3, 10, 600)
    elif lvl2 > lvl1:
        show_text(txt4, 10, 600)
    else:
        show_text(txt5, 10, 600)
    pygame.display.update()
    pygame.time.delay(3000)


def player2win():
    screen.fill(blue)
    screen.blit(background, (0, 0))
    show_text(txt2, 150, 300)
    pygame.display.update()
    pygame.time.delay(2000)


def player1win():
    screen.fill(blue)
    screen.blit(background, (0, 0))
    show_text(txt1, 150, 300)
    pygame.display.update()
    pygame.time.delay(2000)


def show_text(t, x, y):
    text = font1.render(t, True, yellow)
    screen.blit(text, (x, y))


def show_score(x, y):
    Score = font.render("Score:" + str(score), True, black)
    screen.blit(Score, (x, y))


def show_level(a, x, y):
    level = font.render("Level:" + str(a), True, black)
    screen.blit(level, (x, y))


def show_time(t):
    Time = font.render("Time:" + str(t), True, black)
    screen.blit(Time, (670, 0))


def player(x, y):
    screen.blit(playerimg, (x, y))


def obstacle1(x, y):
    screen.blit(obstacle1img, (x, y))


def obstacle2(x, y):
    screen.blit(obstacle2img, (x, y))


def obstacle3(x, y):
    screen.blit(obstacle3img, (x, y))


def obstacle4(x, y):
    screen.blit(obstacle4img, (x, y))


def obstacle5(x, y):
    screen.blit(obstacle5img, (x, y))


def obstacle6(x, y):
    screen.blit(obstacle6img, (x, y))


def isCollision(obstacleX, obstacleY, playerX, playerY):
    distance = math.sqrt(math.pow(obstacleX-playerX, 2) +
                         math.pow(obstacleY-playerY, 2))
    if distance < 27:
        return True
    else:
        return False


pygame.display.set_caption("Across The Atlantic")

# gameloop
while run:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

        if event.type == pygame.KEYDOWN:  # keydown is for checking if key is pressed
            if event.key == ord('a'):  # for left
                playerX_change = -x1
            if event.key == ord('d'):  # for right
                playerX_change = x1
            if event.key == ord('w'):  # for up
                playerY_change = -x1
            if event.key == ord('s'):  # for down
                playerY_change = x1
            if event.key == ord('r'):
                show_result(level1, level2)
            homepage = 0

        if event.type == pygame.KEYUP:  # keyup for checking if key released
            if event.key == ord('a'):
                playerX_change = 0
            if event.key == ord('d'):  # for right
                playerX_change = 0
            if event.key == ord('w'):  # for up
                playerY_change = 0
            if event.key == ord('s'):  # for down
                playerY_change = 0
    if homepage == 1:
        screen.blit(background, (0, 0))
        show_text("Across the Atlantic", 200, 10)
        show_text("Press Any Key to Continue", 150, 750)
        text1 = font.render("Game Rules:", True, black)
        text2 = font2.render(
            "Use 'W' 'A' 'S' 'D' for navigation", True, black)
        text3 = font2.render(
            "'R' for checking game result at any time ", True, black)
        text4 = font2.render(
            "A Player wins if the other player dies or his finalscore(obstacles and", True, black)
        text8 = font2.render(
            "time)  is greater than the other opponent", True, black)
        text5 = font2.render(
            "Passing fixed obstacles means 5 points,moving obstacles mean 10", True, black)
        text6 = font2.render(
            "The greater the time taken, greater is the reduction in finalscore ", True, black)
        text7 = font2.render(
            "At any point in the game the one with the highest level is the current", True, black)
        text9 = font2.render("Winner", True, black)
        screen.blit(text1, (0, 160))
        screen.blit(text2, (0, 200))
        screen.blit(text3, (0, 240))
        screen.blit(text4, (0, 280))
        screen.blit(text8, (0, 320))
        screen.blit(text5, (0, 360))
        screen.blit(text6, (0, 400))
        screen.blit(text7, (0, 440))
        screen.blit(text9, (0, 480))
        pygame.display.update()
        start_time = pygame.time.get_ticks()
    else:
        screen.fill(blue)
        screen.blit(background, (0, 0))

        playerX = playerX + playerX_change
        playerY = playerY + playerY_change

        if count10 % 2 == 0:
            if playerY <= 608:
                score = 10
            if playerY <= 548:
                score = 15
            if playerY <= 488:
                score = 25
            if playerY <= 428:
                score = 30
            if playerY <= 368:
                score = 40
            if playerY <= 308:
                score = 45
            if playerY <= 248:
                score = 55
            if playerY <= 188:
                score = 60
            if playerY <= 128:
                score = 70
        else:
            if playerY >= 640:
                score = 70
            elif playerY >= 580:
                score = 60
            elif playerY >= 520:
                score = 55
            elif playerY >= 460:
                score = 45
            elif playerY >= 400:
                score = 40
            elif playerY >= 340:
                score = 30
            elif playerY >= 280:
                score = 25
            elif playerY >= 220:
                score = 15
            elif playerY >= 160:
                score = 10

        obstacle1X += x1
        obstacle2X += x1
        obstacle3X += x1
        obstacle4X += x1

        obstacle11X = obstacle1X + 100
        obstacle22X = obstacle2X + 100
        obstacle33X = obstacle3X + 100
        obstacle44X = obstacle4X + 100

        obstacle1(obstacle1X, obstacle1Y)
        obstacle2(obstacle2X, obstacle2Y)
        obstacle3(obstacle3X, obstacle3Y)
        obstacle4(obstacle4X, obstacle4Y)
        obstacle5(obstacle5X+100, obstacle5Y)
        obstacle6(obstacle6X+100, obstacle6Y)
        screen.blit(img11, (obstacle11X, 520))
        screen.blit(img21, (obstacle22X, 520))
        screen.blit(img31, (obstacle33X, 520))
        screen.blit(img41, (obstacle44X, 520))
        screen.blit(img51, (obstacle5X, 460))
        screen.blit(img61, (obstacle6X, 460))
        screen.blit(img12, (obstacle1X, 400))
        screen.blit(img22, (obstacle2X, 400))
        screen.blit(img32, (obstacle3X, 400))
        screen.blit(img42, (obstacle4X, 400))
        screen.blit(img52, (obstacle5X+100, 340))
        screen.blit(img62, (obstacle6X+100, 340))
        screen.blit(img13, (obstacle11X, 280))
        screen.blit(img23, (obstacle22X, 280))
        screen.blit(img33, (obstacle33X, 280))
        screen.blit(img43, (obstacle44X, 280))
        screen.blit(img53, (obstacle5X, 220))
        screen.blit(img63, (obstacle6X, 220))
        screen.blit(img14, (obstacle1X, 160))
        screen.blit(img24, (obstacle2X, 160))
        screen.blit(img34, (obstacle3X, 160))
        screen.blit(img44, (obstacle4X, 160))

        show_score(textX, textY)
        show_level(a, 300, 10)

        if obstacle1X >= 736:
            obstacle1X = 0
        if obstacle2X >= 736:
            obstacle2X = 0
        if obstacle3X >= 736:
            obstacle3X = 0
        if obstacle4X >= 736:
            obstacle4X = 0
        if obstacle11X >= 736:
            obstacle11X = 0
        if obstacle22X >= 736:
            obstacle22X = 0
        if obstacle33X >= 736:
            obstacle33X = 0
        if obstacle44X >= 736:
            obstacle44X = 0

        if playerX <= 0:
            playerX = 0
        elif playerX >= 736:
            playerX = 736
        if playerY <= 0:
            playerY = 0
        elif playerY >= 736:
            playerY = 736
        player(playerX, playerY)
        show_time(math.ceil((pygame.time.get_ticks()-start_time)/1000))

        if (isCollision(obstacle1X, obstacle1Y, playerX, playerY) or
            isCollision(obstacle2X, obstacle2Y, playerX, playerY) or
            isCollision(obstacle3X, obstacle3Y, playerX, playerY) or
            isCollision(obstacle4X, obstacle4Y, playerX, playerY) or
            isCollision(obstacle5X+100, obstacle5Y, playerX, playerY) or
            isCollision(obstacle6X+100, obstacle6Y, playerX, playerY) or
            isCollision(obstacle11X, 520, playerX, playerY) or
            isCollision(obstacle22X, 520, playerX, playerY) or
            isCollision(obstacle33X, 520, playerX, playerY) or
            isCollision(obstacle44X, 520, playerX, playerY) or
            isCollision(obstacle5X, 460, playerX, playerY) or
            isCollision(obstacle6X, 460, playerX, playerY) or
            isCollision(obstacle1X, 400, playerX, playerY) or
            isCollision(obstacle2X, 400, playerX, playerY) or
            isCollision(obstacle3X, 400, playerX, playerY) or
            isCollision(obstacle4X, 400, playerX, playerY) or
            isCollision(obstacle5X+100, 340, playerX, playerY) or
            isCollision(obstacle6X+100, 340, playerX, playerY) or
            isCollision(obstacle11X, 280, playerX, playerY) or
            isCollision(obstacle22X, 280, playerX, playerY) or
            isCollision(obstacle33X, 280, playerX, playerY) or
            isCollision(obstacle44X, 280, playerX, playerY) or
            isCollision(obstacle5X, 220, playerX, playerY) or
            isCollision(obstacle6X, 220, playerX, playerY) or
            isCollision(obstacle1X, 160, playerX, playerY) or
            isCollision(obstacle2X, 160, playerX, playerY) or
            isCollision(obstacle3X, 160, playerX, playerY) or
            isCollision(obstacle4X, 160, playerX, playerY) or
                (playerY <= 0 and count10 % 2 == 0) or (playerY >= 730 and count10 % 2 == 1)):
            if count10 % 2 == 0:
                screen.fill(blue)
                screen.blit(background, (0, 0))
                show_text(txt6, 150, 300)
                pygame.display.update()
                pygame.time.delay(2000)
                playerimg = pygame.image.load('ship.png')
                playerimg = pygame.transform.scale(playerimg, (32, 32))
                player1t = math.ceil((pygame.time.get_ticks()-start_time)/1000)
                player1s = score + 10000-player1t
                if playerY <= 0:
                    flag = 1
                flag1 = 0
                x1 = level2
                a = level2
                playerX = 370
                playerY = 70
            else:
                playerimg = pygame.image.load('boat.png')
                playerimg = pygame.transform.scale(playerimg, (32, 32))
                player2t = math.ceil((pygame.time.get_ticks()-start_time)/1000)
                player2s = score + 10000-player2t
                if playerY >= 720:
                    flag1 = 1
                if flag1 != 0 and flag != 0:
                    if player2s > player1s:
                        player2win()
                        level2 += 1
                    elif player1s > player2s:
                        player1win()
                        level1 += 1
                    else:
                        screen.fill(blue)
                        screen.blit(background, (0, 0))
                        show_text(txt7, 150, 300)
                        pygame.display.update()
                        pygame.time.delay(2000)
                        level1 += 1
                        level2 += 1
                elif flag1 == 0 and flag != 0:
                    player1win()
                    level1 += 1
                    flag1 = 1
                elif flag == 0 and flag1 != 0:
                    player2win()
                    level2 += 1
                    flag = 1
                else:
                    screen.fill(blue)
                    screen.blit(background, (0, 0))
                    show_text(txt8, 150, 300)
                    pygame.display.update()
                    pygame.time.delay(2000)
                x1 = level1
                a = level1
                flag = 0
                playerX = 370
                playerY = 720
                screen.fill(blue)
                screen.blit(background, (0, 0))
                show_text(txt9, 150, 300)
                pygame.display.update()
                pygame.time.delay(2000)

            start_time = pygame.time.get_ticks()
            count10 += 1
            score = 0
        pygame.display.update()
